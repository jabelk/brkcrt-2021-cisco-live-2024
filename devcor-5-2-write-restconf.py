import requests, json
from requests.auth import HTTPBasicAuth
url = "https://devnetsandboxiosxe.cisco.com:443/restconf/data/ietf-interfaces:interfaces"
payload = json.dumps({"ietf-interfaces:interface": {
        "name": "Loopback101",
        "description": "Configured by RESTCONF",
        "type": "iana-if-type:softwareLoopback",
        "enabled": True,
        "ietf-ip:ipv4": {
            "address": [{
                    "ip": "172.15.100.2",
                    "netmask": "255.255.255.0"}]}}})
headers = {'Accept': 'application/yang-data+json',
'Content-Type': 'application/yang-data+json'}
session = requests.Session()
session.auth = HTTPBasicAuth('admin', 'C1sco12345')
session.verify = False 
response = session.post(url, headers=headers, data=payload)
print(response.text)
