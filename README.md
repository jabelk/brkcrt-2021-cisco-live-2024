# brkcrt-2021 Cisco Live 2024

## Cisco DEVCOR Certification Blueprint Repository

This repository contains examples and code snippets that align with the Cisco DEVCOR certification blueprint topics. Each section corresponds to a specific topic within the certification, providing resources and links to relevant files organized by the topics covered.

## Table of Contents

- [1.12 Construct a Sequence Diagram that Includes API Calls](#112-construct-a-sequence-diagram-that-includes-api-calls)
- [4.1 Diagnose a CI/CD Pipeline Failure](#41-diagnose-a-cicd-pipeline-failure)
- [5.2 Utilize RESTCONF to Configure a Network Device](#52-utilize-restconf-to-configure-a-network-device)
- [5.3 Construct a Workflow to Configure Network Parameters with Ansible](#53-construct-a-workflow-to-configure-network-parameters-with-ansible)

### 1.12 Construct a Sequence Diagram that Includes API Calls

This section covers the construction of sequence diagrams that incorporate API calls, essential for understanding and documenting the interactions in a system.

- ![Sequence Diagram](seq-final-rest.png)
- [PlantUML File](devcor-1-12.puml)

### 4.1 Diagnose a CI/CD Pipeline Failure

Understanding how to diagnose failures in CI/CD pipelines is critical for maintaining continuous integration and deployment processes.

- [GitLab CI Configuration](.gitlab-ci.yml)
example: https://gitlab.com/jabelk/brkcrt-2021-cisco-live-2024/-/jobs/6663597620
```
$ ansible-playbook -i inventory.ini pb-conf-snmp.yaml
[WARNING]: Ansible is being run in a world writable directory
(/builds/jabelk/brkcrt-2021-cisco-live-2024), ignoring it as an ansible.cfg
source. For more information see
https://docs.ansible.com/ansible/devel/reference_appendices/config.html#cfg-in-
world-writable-dir
PLAY [PLAY 1 - DEPLOYING SNMP CONFIGURATIONS ON IOS] ***************************
TASK [TASK 2 in PLAY 1 - VERIFY SNMP LINES PRESENT] ****************************
fatal: [csr1]: FAILED! => {"changed": false, "msg": "\nlibssh: The authenticity of host 'devnetsandboxiosxe.cisco.com' can't be established due to 'Host is unknown: 6b:bb:99:f7:93:76:57:63:f2:84:02:49:1f:91:03:d6:1f:40:8e:6c'.\nThe ssh-rsa key fingerprint is SHA1:a7uZ95N2V2PyhAJJH5ED1h9Ajmw."}
PLAY RECAP *********************************************************************
csr1                       : ok=0    changed=0    unreachable=0    failed=1    skipped=0    rescued=0    ignored=0   
Cleaning up project directory and file based variables
00:01
ERROR: Job failed: exit code 1
```

### 5.2 Utilize RESTCONF to Configure a Network Device

Demonstrates how to use RESTCONF for configuring network devices such as interfaces, static routes, and VLANs specific to IOS XE.

- [RESTCONF Python Script](devcor-5-2-write-restconf.py)

### 5.3 Construct a Workflow to Configure Network Parameters with Ansible

Using Ansible to automate network configuration tasks is an efficient way to manage network parameters.

- [Ansible Playbook](pb-conf-snmp.yaml)
- [Ansible Configuration File](ansible.cfg)
- [Inventory File](inventory.ini)

---

For more information on Cisco's certification and additional resources, visit the [official Cisco certification page](https://learningnetwork.cisco.com/s/devcor-exam-topics).
